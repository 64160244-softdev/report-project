/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.reportproject;

import java.util.List;

/**
 *
 * @author thanc
 */
public class ArtistService {

    ArtistDao artistDao = new ArtistDao();

    public List<ArtistReport> getTopTenArtistByTotalPrice() {
        return artistDao.getArtistByTotalPrice(10);
    }

    public List<ArtistReport> getTopTenArtistByTotalPrice(String begin, String end) {
        return artistDao.getArtistByTotalPrice(begin, end, 10);
    }
}
